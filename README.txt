Библиотека надстройка над libusb-1.0 для простого подключения к 
моим USB-HID устройствам для программ на Qt

Требуется:
    * >=Qt-4.8.0
    * >=cmake-4.8
	* libmylog (https://bitbucket.org/Olololshka/libmylog)
	* libsettingscmdline (https://bitbucket.org/Olololshka/libsettingscmdline)
    * >=doxygen-1.8 (Документация)
    * >=git-1.7 (Получение исходных кодов)

Сборка:
    * Скачать исхохный код при помощи git
    $ git clone git clone https://bitbucket.org/Olololshka/libmyusbhiddevice.git

    * Создаем каталог для сборки
    $ mkdir libqmodbus/build && cd libqmodbus/build

    * запуск cmake
    ** для windows:
            $ cmake .. -G"MSYS Makefiles"
    ** для Linux:
            $ cmake ..
      В процессе могут возниукнуть ошибки, по причине нехватки некоторых обязательных компанентов.
      Воспользуйтесь документацией чтобы устранить эти ошибки.

    * Сборка
    make

    * Установка (для Linux требуются права администратора).
      Каталог установки по-умолчанию /usr/local, что соответствует
      <каталог установки MinGW>/msys/1.0/local в Windows
    # make install

    * Сборка документации
    $ make doc


